<?php
// Routes

require __DIR__ . '/../public/MyRestfulController.php';

$app->any('/user', 'MyRestfulController:index');

//$app->get('/hellos', '\App\Controller\HelloController:index');

$app->get('/hello', function ($request, $response, $args) {
	// Sample log message
	$this->logger->info("Slim-Skeleton '/' route");
	// Render index view
	return $this->renderer->render($response, 'test.phtml', $args);
});

$app->get('/[{name}]', function ($request, $response, $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/' route");
    // Render index view
    return $this->renderer->render($response, 'index.phtml', $args);
});
