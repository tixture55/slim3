<?php

	class Calculation
	{
		public $value1 = 10;
		public $value2 = 20;

		//getterメソッド
		public function getValue1() { return $this->value1; }

		//setterメソッド
		public function setValue1($new_val) { $this->value1 = $new_val; }


		//計算メソッド
		public function add($val1)
		{
			$value1 = $this->value1 + $val1;

			return $value1;
		}
	}

