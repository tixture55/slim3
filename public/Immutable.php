<?php

class Immutable{
    private $obj;
    private $setters_pattern;

    public function __construct( $obj, $setters_pattern = '/set([0-9])*/' ){
        $this->obj = $obj;
        $this->setters_pattern = $setters_pattern;
    }

    private function checkSetters( $name ){
        if ( is_string($this->setters_pattern) ){
            return preg_match( $this->setters_pattern, $name );


        }
        else if ( is_array($this->setters_pattern) ){
            foreach( $this->setters_pattern as $pattern ){
                if ( preg_match( $pattern, $name ) )    return TRUE;
            }
            return FALSE;
        }
        return FALSE;
    }
 public function __get( $key ){
        // 内包オブジェクトに対応するプロパティがある場合は取得
        if ( property_exists( $this->obj, $key ) ){
            return $this->obj->$key;
        }
        // えっ！？
        throw new LogicException('えっ！？');
    }

    public function __set( $key, $value ){
        // $foo->barでプロパティを設定した場合は例外をスロー
        throw new Exception( "Immutableオブジェクトのため変更できません:$key" );
    }

    public function __call($name, $arguments){
        // setters_patternにマッチしたメソッドは例外をスロー
        if ( $this->checkSetters( $name ) ){
            throw new Exception( "Immutableオブジェクトのため変更できません:$name" );
        }
        // 内包オブジェクトに対応するメソッドがある場合は実行
        else if ( method_exists( $this->obj, $name ) ){
            return call_user_func_array( array($this->obj,$name), $arguments );
        }
        // えっ！？
        throw new LogicException($name. ' does not exist function.');
    }
}
